﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Latin_Translator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            EnglishLeft.Visibility = Visibility.Hidden;
            EnglishRight.Visibility = Visibility.Hidden;
            EnglishCenter.Visibility = Visibility.Hidden;
        }

        private void Left_Click(object sender, RoutedEventArgs e)
        {
            EnglishLeft.Visibility = Visibility.Visible;
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            EnglishRight.Visibility = Visibility.Visible;
        }

        private void Center_Click(object sender, RoutedEventArgs e)
        {
            EnglishCenter.Visibility = Visibility.Visible;
        }
    }
}
